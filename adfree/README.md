---
title: AdFree
---

### Use Content Security Policy(CSP)

::: tip
Content Security Policy (CSP) is an added layer of security that helps to detect and mitigate certain types of attacks, including Cross Site Scripting (XSS) and data injection attacks. These attacks are used for everything from data theft to site defacement to distribution of malware.
:::

#### Configuration Files
* csp.js

```js
var meta = document.createElement('meta');
meta.httpEquiv = 'Content-Security-Policy';
meta.content = "default-src 'self'; script-src 'self' 'unsafe-inline'; style-src 'self' 'unsafe-inline'; img-src 'self'; connect-src 'self'; child-src 'self'; object-src 'self'";
document.getElementsByTagName('head')[0].appendChild(meta);
```

```js
<script src="csp.js"></script>
```

### Use common javscript

::: tip
Remove tag that xrea added.
:::

#### Configuration Files
* xrea_footer.js

```js
(function(){
  var func = function(){
    var newBox = document.getElementsByTagName("div")[0];
    if (newBox === null) {return false;}
    if(newBox.id === "vdbanner"){newBox.parentNode.removeChild(newBox);};
  };

  try{
    window.addEventListener("load", func, false);
  }
  catch(e){
    window.attachEvent("onload",func);
  }
})();
```

```js
<script src="xrea_footer.js"></script>
```
