---
title: Customizing Your Bash Shell
---
## Change login shell
```bash
chsh -s /bin/bash
```

## Add aliases
Create aliases for commands or tasks that we do often or for long, complex command that may be useful in the future.

::: tip
The ~/.bash_profile file should contain anything you want run when you first log into an interactive Bash Shell.
:::

#### Configuration Files
* ~/.bash_profile

```bash
export PATH=$PATH:~/bin

# Change php version
php_bin="php72"
php_cfg="~/public_html/.fast-cgi-bin/${php_bin}.ini"
php_cli="${php_bin}cli"

# Add alias
alias php="${php_bin} -c ${php_cfg}"
alias phpcli="${php_cli}"
```
