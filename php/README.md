---
title: PHP.INI File Configuration
---
## php extension configuration

::: tip
The php.ini file is the default configuration file for running applications that require PHP. It is used to control variables such as upload sizes, file timeouts, and resource limits, etc.
:::

### Configuration Files
* ~/public_html/.fast-cgi-bin/php72.ini

#### Install opcache

```bash
cat>>~/public_html/.fast-cgi-bin/php72.ini<<EOF
zend_extension=php72_opcache.so
opcache.enable=1
opcache.enable_cli=1
opcache.memory_consumption=128
opcache.interned_strings_buffer=8
opcache.max_accelerated_files=10000
opcache.save_comments=1
opcache.revalidate_freq=1
EOF
```

#### Addition modules

Download php72_opcache.so, xmlreader.so, xmlwriter.so, intl.so to directory ~/bin.

```bash
cat>>~/public_html/.fast-cgi-bin/php72.ini<<EOF
extension=xmlreader.so
extension=xmlwriter.so
extension=intl.so
EOF

sed -i "s@/usr/local/lib/php/extensions/@${basepath}/extensions/php72/@' ~/public_html/.fast-cgi-bin/php72.ini
```
