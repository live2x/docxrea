---
home: true
heroImage: /logo.png 
actionText: Get Started →
actionLink: /bash/
pageClass: custom-home-class
features:
- title: Free 
  details: High-function, high-quality server that can be used free.
- title: All plan Corresponding to latest CPU, SSD 
  details: By adopting the latest CPU and SSD in the server environment, it will be possible to further speed up and stabilize.
- title: API
  details: By using the API, it becomes possible to link with other services or to create and publish their own applications. 
footer: Copyright © 2018 Xrea Inc.
---
