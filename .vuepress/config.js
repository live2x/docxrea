module.exports = {
  title: 'Free Hosting',
  description: 'Customizing runtime environment',
  head: [
    ['meta', {
      'http-equiv': 'Content-Security-Policy',
      'content': "default-src 'self'; script-src 'self' 'unsafe-inline'; style-src 'self' 'unsafe-inline'; img-src 'self'; connect-src 'self'; child-src 'self'; object-src 'self'"      
    }]
  ],
  markdown: {
    lineNumbers: true
  },
  themeConfig: {
    displayAllHeaders: false,
    activeHeaderLinks: true,
    nav: [
      { text: 'Home', link: '/' },
      { text: 'Guide', link: '/bash/' },
      { text: 'External', link: 'https://www.xrea.com' },
    ],
    sidebar: [
      ['/', 'Home'], 
      ['/bash/', 'BASH'],
      ['/php/', 'PHP'],
      ['/composer/', 'Composer'],
      ['/vim/', 'VIM'],
      ['/nodejs/', 'NodeJS'],
      ['/vue/', 'Vue'],
      ['/vuepress/', 'VuePress'],
      ['/adfree/', 'AdFree']
    ],
    lastUpdated: 'Last Updated',
  }
}
