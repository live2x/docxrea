---
title: Customize Vim Instance
---
## Set encoding and fileencoding

::: tip
During Vim's loading sequence, it will automatically check the current user's home directory for a .vimrc file. All settings specified in this file will override explicitly contradicted settings in any previously loaded config files, which in this case is the global vimrc file.
:::

#### Configuration Files
* ~/.vimrc

```bash
# Change the output encoding of the file that is written
set fileencodings=utf-8,gb2312,gbk,gb18030
set termencoding=utf-8
set fileformats=unix
# Change the output encoding that is shown in the terminal
set encoding=utf-8
```
