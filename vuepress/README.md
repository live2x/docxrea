---
title: Setup VuePress
---
### Setting up VuePress development environment

::: tip
Minimalistic docs generator with Vue component based layout system
:::

```bash
# install globally
yarn global add vuepress

# add alias
cat>>~/.bash_profile<<EOF
alias vuepress="node ~/nodejs/bin/vuepress"
EOF

source ~/.bash_profile

# build
vuepress build
```
