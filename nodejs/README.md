---
title: Node.js Environment Setup
---
## Install npm and yarn

::: tip
npm is the package manager for JavaScript and the world’s largest software registry. Discover packages of reusable code.

Yarn is a package manager for your code. It allows you to use and share code with other developers from around the world. Yarn does this quickly, securely, and reliably so you don’t ever have to worry.
:::

#### Configuration Files
* ~/.bash_profile

```bash
#!/bin/bash

cd ~
basepath=$(cd `dirname $0`;pwd)

# install nodejs
wget https://nodejs.org/dist/v10.13.0/node-v10.13.0-linux-x64.tar.xz
tar -xvf node-v10.13.0-linux-x64.tar.xz
rm -f node-v10.13.0-linux-x64.tar.xz
mv node-v10.13.0-linux-x64 nodejs

# Add alias
cat>>~/.bash_profile<<EOF
alias node="~/nodejs/bin/node"
alias npm="node ~/nodejs/bin/npm"
alias npx="node ~/nodejs/bin/npx"
EOF

source ~/.bash_profile

# Set nodejs config directory
npm set prefix="~/nodejs"

# install yarn
npm install -g yarn
cat>>~/.bash_profile<<EOF
alias yarn="node ~/nodejs/bin/yarn"
alias yarnpkg="node ~/nodejs/bin/yarnpkg"
EOF

source ~/.bash_profile
```
