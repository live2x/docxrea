---
title: Install Composer
---
Install Composer In Your Project

```bash
cd ~

mkdir bin

# Download composer
curl -sS https://getcomposer.org/installer | php
mv composer.phar composer

# Add alias
cat>>~/.bash_profile<<EOF
alias composer="phpcli ~/bin/composer"
EOF
```

Use composer

```bash
composer -v
```
