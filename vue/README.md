---
title: Setup Vue
---
### Setting up Vue.js development environment

::: tip
Vue is a progressive framework for building user interfaces. It is designed from the ground up to be incrementally adoptable, and can easily scale between a library and a framework depending on different use cases. It consists of an approachable core library that focuses on the view layer only, and an ecosystem of supporting libraries that helps you tackle complexity in large Single-Page Applications.
:::

### Installation

### Configuration Files
* vue.sh

```bash
# install vue cli
yarn global add @vue/cli

# add alias
cat>>~/.bash_profile<<EOF
alias vue="node ~/nodejs/bin/vue"
EOF

source ~/.bash_profile
```

### Use Vue

```bash
# Create a project  and manage packages by yarn
vue create myapp -m yarn

# Build project
yarn run build 
```
